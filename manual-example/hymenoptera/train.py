import logging
from configparser import ConfigParser
import os
import os.path as path
import torch as t
from torch.utils.data import DataLoader
from torchvision import datasets, models, transforms

import pytorch_lightning as pl
from pytorch_lightning import Trainer

DATAROOT = os.getenv("DATAROOT", "/Users/avilay/mldata/hymenoptera_data")
LOGDIR = os.getenv("LOGDIR", "/Users/avilay/tblogs/hymenoptera")
RUN_ID = os.getenv("RUN_ID", "dev-run")
FULL_RUN = os.getenv("FULL_RUN", "")
HPARAMS_FILE = os.getenv(
    "HPARAMS_FILE", "/Users/avilay/projects/gitlab-personal/k8-pytorch/hparams/trial-1.ini"
)

logformat = "%(asctime)s:%(levelname)s:%(name)s:%(message)s"
logging.basicConfig(format=logformat, level=logging.DEBUG)


class Hyperparams:
    def __init__(self, **kwargs):
        self.batch_size = kwargs["batch_size"]
        self.epochs = kwargs["epochs"]
        self.lr = kwargs["lr"]
        self.lr_step_size = kwargs["lr_step_size"]
        self.gamma = kwargs["gamma"]

    def __repr__(self):
        return f"<Hyperparams(batch_size={self.batch_size} epochs={self.epochs} lr={self.lr} lr_step_size={self.lr_step_size} gamma={self.gamma})>"

    @classmethod
    def load(cls, inifile):
        hparams = ConfigParser()
        hparams.read(inifile)
        return cls(
            batch_size=hparams["DEFAULT"].getint("batch_size"),
            epochs=hparams["DEFAULT"].getint("epochs"),
            lr=hparams["DEFAULT"].getfloat("lr"),
            lr_step_size=hparams["DEFAULT"].getint("lr_step_size"),
            gamma=hparams["DEFAULT"].getfloat("gamma"),
        )


class ImageClassifier(pl.LightningModule):
    def __init__(self, hparams):
        super().__init__()
        self.hparams = hparams

        self.loss_fn = t.nn.CrossEntropyLoss()

        self.means = [0.485, 0.456, 0.406]
        self.stds = [0.229, 0.224, 0.225]

        model = models.resnet18(pretrained=True)

        # The last layer is called fc which is a Linear layer with input (512,) and output (1000,)
        # This needs to be replaced with another Linear layer with the same input, but output of (2,)
        num_features = model.fc.in_features
        model.fc = t.nn.Linear(num_features, 2)

        self.resnet = model

    def configure_optimizers(self):
        optim = t.optim.SGD(self.resnet.parameters(), lr=self.hparams.lr, momentum=0.9)
        exp_lr = t.optim.lr_scheduler.StepLR(
            optim, step_size=self.hparams.lr_step_size, gamma=self.hparams.gamma
        )
        return [optim], [exp_lr]

    def forward(self, images):
        return self.resnet(images)

    def training_step(self, batch, batch_num):
        images, targets = batch
        outputs = self.forward(images)
        loss = self.loss_fn(outputs, targets)
        tbmetrics = {"train_loss": loss}
        return {"loss": loss, "log": tbmetrics}

    def validation_step(self, batch, batch_num):
        images, targets = batch
        outputs = self.forward(images)
        loss = self.loss_fn(outputs, targets)
        predictions = t.argmax(outputs, dim=1)
        correct = t.sum(predictions == targets.data)
        batch_size = len(images)
        return {"loss": loss, "correct": correct, "batch_size": batch_size}

    def validation_end(self, outputs):
        losses = []
        corrects = []
        batch_sizes = []
        for output in outputs:
            losses.append(output["loss"])
            corrects.append(output["correct"])
            batch_sizes.append(output["batch_size"])
        avg_loss = t.stack(losses).mean()
        avg_acc = t.stack(corrects).sum() / sum(batch_sizes)
        return {"val_loss": avg_loss, "log": {"val_loss": avg_loss, "val_acc": avg_acc}}

    @pl.data_loader
    def train_dataloader(self):
        train_xforms = transforms.Compose(
            [
                transforms.RandomResizedCrop(224),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(self.means, self.stds),
            ]
        )
        train_datadir = path.join(DATAROOT, "train")
        traindata = datasets.ImageFolder(train_datadir, train_xforms)
        return DataLoader(traindata, batch_size=self.hparams.batch_size, shuffle=True)

    @pl.data_loader
    def val_dataloader(self):
        val_xforms = transforms.Compose(
            [
                transforms.Resize(256),
                transforms.CenterCrop(224),
                transforms.ToTensor(),
                transforms.Normalize(self.means, self.stds),
            ]
        )
        val_datadir = path.join(DATAROOT, "val")
        valdata = datasets.ImageFolder(val_datadir, val_xforms)
        return DataLoader(valdata, batch_size=self.hparams.batch_size)


def main():
    hparams = Hyperparams.load(HPARAMS_FILE)
    logging.info(f"Starting run with hparams: {hparams}")
    model = ImageClassifier(hparams)
    trainer = Trainer(
        show_progress_bar=False,
        default_save_path=path.join(LOGDIR, RUN_ID),
        max_nb_epochs=hparams.epochs,
    )
    if t.cuda.is_available():
        trainer.gpus = 1
        logging.info("Using GPU")
    if not FULL_RUN:
        trainer.fast_dev_run = True
        logging.info("Doing a fast run")
    trainer.fit(model)


if __name__ == "__main__":
    main()
